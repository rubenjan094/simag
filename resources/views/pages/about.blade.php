@extends('layouts.app')

@section('content')

    <div class="about">
        <div class="about-banner" style="
            width: 100%;
            height: 350px;
            background-image: url({{asset('/img/about-page.jpg')}});
            background-position: center;
            background-size: cover;
            background-blend-mode: color;
            background-color: #000000c9;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            ">
            <div class="container">
                <h1 data-aos="fade-up">About Us</h1>
                <p data-aos="fade-up">WHEN QUALITY MEETS EXELLENCES</p>
            </div>
        </div>
        <div class="about-content">
            <div class="container">
                <p data-aos="fade-up">
                    SIMAG LLC is one of the leading companies in Armenia in the field of production of chemicals through
                    innovative technologies. The company was established in 2018 on the basis of "Ecoatom" company,
                    which
                    has extensive experience in the field of research and development of industrial technologies.
                </p>
                <p data-aos="fade-up">
                    Armenian
                    land is rich of quartzite mines in different regions and SIMAG LLC has received a license for
                    operating
                    on Gnishik quartzite mine in Vayots Dzor, which contains up to 96.7%s silicon stocks. Currently, the
                    company has a silicon dioxide factory in Yerevan and a mining point in Vayots Dzor.
                </p>
                <p data-aos="fade-up">
                    SIMAG’s vision is to
                    be high purity silicon dioxide producer globally. Our goal is to provide our partners high quality
                    production.
                </p>
                <p data-aos="fade-up">We strive to showcase our <span>core values</span> within all of our interactions.</p>
            </div>
        </div>
        <div class="about-values section">
            <h3>CORE VALUES</h3>
            <div class="container">
                <div class="row">
                    <div class="col-md-4" data-aos="flip-left"
                         data-aos-easing="ease-out-cubic"
                         data-aos-duration="1000">
                        <div class="about-values-item">
                            <img src="{{ asset('/img/value.svg') }}" alt="" width="100%">
                            <h4>Safety & Wellness</h4>
                            <p>Employees safety and well-being playsa vital role for our company therefore we have
                                developed a package to prevent any work related injuries and practice injury-free habits
                                at home and at work.</p>
                        </div>
                    </div>
                    <div class="col-md-4" data-aos="flip-left"
                         data-aos-easing="ease-out-cubic"
                         data-aos-duration="1000">
                        <div class="about-values-item">
                            <img src="{{ asset('/img/value.svg') }}" alt="" width="100%">
                            <h4>Performance Driven Team</h4>
                            <p>Every day we enlarge our capacities through precise collaboration withall our partners.
                                Our actions are driven by customer insightsand helping themcreate value.</p>
                        </div>
                    </div>
                    <div class="col-md-4" data-aos="flip-left"
                         data-aos-easing="ease-out-cubic"
                         data-aos-duration="1000">
                        <div class="about-values-item">
                            <img src="{{ asset('/img/value.svg') }}" alt="" width="100%">
                            <h4>Innovation</h4>
                            <p>We act as owners of the company with a bias for action and a can-do attitude. We
                                contribute our best to the team and accept personal responsibility for our performance
                                and development.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="about-reserch section"  data-aos="fade-up">
            <h3>Research Lab</h3>
            <div class="about-reserch-content">
                <div class="container">
                    <p>
                        We are oriented on valuing analysis of complex (multistep) chemical reactions.
                    </p>
                    <p>
                        Our researchers team has
                        developed numerous industrial technologies with environmentally safe applications in the fields of
                        renewable energy, hydro-metallurgy, nuclear wastes processing, and recycling of industrial
                        wastes.
                    </p>
                    <p>
                        On the
                        other hand, we have developed advanced and environmentally sound method of electrodialysis and
                        electrolysis with especially selected components, on the basis of which several systems are designed and
                        prototype.
                    </p>
                </div>
            </div>
        </div>
        <div class="about-certificates section">
            <h3>Certificates</h3>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6"  data-aos="fade-right">
                        <img src="{{ asset('/img/certificate.png') }}" width="100%" alt="">
                    </div>
                    <div class="col-md-6"  data-aos="fade-left">
                        <h4>Our production has been certified by Agilent Trusted.</h4>
                        <p>Excellent detection limits were achieved for the requested elements.</p>
                        <p>Internal standards show excellent stability with the SiO2 sample matrix</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="about-partners section">
            <h3>Our Partners</h3>
            <div class="container">
                <div class="row"  data-aos="fade-up">
                    <div class="col-md-4">
                        <img src="{{ asset('/img/partner.png') }}" width="100%" alt="">
                    </div>
                    <div class="col-md-4">
                        <img src="{{ asset('/img/partner2.png') }}" width="100%" alt="">
                    </div>
                    <div class="col-md-4">
                        <img src="{{ asset('/img/partner3.png') }}" width="100%" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
