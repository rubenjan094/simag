@extends('layouts.app')
@section('content')


    <section class="project-heading">
        <div style="background: url({{ asset('/img/projects.png') }});
            width: 100%;
            height: 400px;
            background-position: center;
            background-size: cover;
            background-blend-mode: color;
            background-color: #00000091;
            display: flex;
            align-items: center;
            justify-content: center;
            ">
            <h1 class="text-center text-white">OUR PROJECTS</h1>
        </div>
    </section>
    <section class="projects container">
        <div class="row">
            @for($i = 0; $i < 8; $i++)
                <div class="projects-item" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                    <div class="col-md-12">
                        <div class="row align-items-center">
                            <div class="col-md-4 p-0">
                                <img src="{{ asset('/img/project2.webp') }}" width="100%" alt="">
                            </div>
                            <div class="col-md-8">
                                <h3>17 Proven Ways to Promote Your YouTube Channel</h3>
                                <hr>
                                <p>With more than 2 billion monthly active users, YouTube is the second largest social
                                    media
                                    in
                                    the world. Millions of users worldwide consume one billion hours of video every day,
                                    making
                                    it an ideal place to be if you want to promote your brand or product in digital
                                    space.</p>
                            </div>
                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="staticBackdrop" style="top: 80px !important;" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog  modal-dialog-scrollable">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="staticBackdropLabel">17 Proven Ways to Promote Your YouTube Channel</h5>
                                    <button type="button" class="btn df-btn" data-bs-dismiss="modal" aria-label="Close">X</button>
                                </div>
                                <div class="modal-body">
                                     <img src="{{ asset('/img/project2.webp') }}" width="100%" class="mb-2" alt="">
                                    Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.

                                    Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.

                                    Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.

                                    Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.

                                    Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endfor
        </div>
    </section>

@endsection
