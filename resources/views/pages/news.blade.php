@extends('layouts.app')

@section('content')


    <div class="news">
        <div class="news-banner" style="
            background-image: url({{asset('/img/news.jpg')}});
            width: 100%;
            height: 400px;
            background-position: center;
            background-size: cover;
            background-blend-mode: color;
            background-color: rgba(0,0,0,0.71);
            display: flex;
            justify-content: center;
            align-items: center;
            margin-bottom: 35px;
            ">
            <h1 class="text-center text-white">NEWS</h1>
        </div>
        <div class="news-container container">
            <div class="row">

                @for($i = 0; $i < 8; $i ++)
                    <div class="col-md-4">
                        <div class="news-container-item">
                            <iframe width="100%" height="315" src="https://www.youtube.com/embed/hWEDJ58Jrqg"
                                    frameborder="0"
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                            <h4>News Title</h4>
                            <?php $text = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem, sapiente, sunt?
                                Aperiamtque deleniti dignissimos, dolorem est, et fugit iste laboriosam minima
                                mollitia
                                nulla perspiciatis quidem quis sequi unde velit?  atque deleniti dignissimos, dolorem est, et fugit iste laboriosam minima
                                mollitia
                                nulla perspiciatis quidem quis sequi unde velit?'; ?>
                            <p><?=substr($text, 0, 200)?>
                            <p class="news-desc-more">
                                mollitia
                                nulla perspiciatis quidem quis sequi unde velit? atque deleniti dignissimos, dolorem
                                est, et fugit iste laboriosam minima
                                mollitia
                                nulla perspiciatis quidem quis sequi unde velit?
                                nulla perspiciatis quidem quis sequi unde velit? atque deleniti dignissimos, dolorem
                                est, et fugit iste laboriosam minima
                                mollitia
                                nulla perspiciatis quidem quis sequi unde velit?
                            </p>
                            <?php if(strlen($text) > 200) { ?>
                            <a href="" class="d-block text-center news-load-more df-btn">Read More</a>
                            <?php } ?>
                        </div>
                    </div>
                @endfor

            </div>
        </div>
    </div>


@endsection
