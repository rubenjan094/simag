@extends('layouts.app')

@section('content')

<div class="contact">
    <div class="contact-banner" style="
        background: url({{asset('/img/contact.jfif')}});
        width: 100%; height: 300px;
        background-position: center;
        background-size: cover;
        display: flex;
        align-items: center;
        justify-content: center;
        background-blend-mode: color;
        background-color: rgba(0,0,0,0.71);
        ">
        <h1 class="text-center text-white">CONTACT US</h1>
    </div>
    <div class="container">
        <p class="text-center mt-5">Contact us page should contain general contact information on SIMAG and give an opportunity to send a request to the company</p>
        <p class="text-center"><b>“SIMAG” LLS</b></p>
        <hr>
        <p class="text-center">7 Tadevosyan St, Yerevan, Armenia</p>
        <p class="text-center"><b>Phone: </b> +374 93 723093, +374 91 275751</p>
        <p class="text-center"><b>Email: </b> simag.ainfo@gmail.com</p>
        <div class="row justify-content-center">
            <div class="col-md-9">
                <div class="d-flex contact-block">
                    <div  data-aos="flip-right">
                        <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3Aefe8757db348fb625d78f06391d298df3e41f0dcb3f1c63394d21e0b699fbdb5&amp;source=constructor" width="346" height="455" frameborder="0"></iframe>
                    </div>
                    <div class="contact-block-form"  data-aos="flip-left">
                        <div class="form-group">
                            <input type="text" placeholder="Name Surname" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="email" placeholder="Email" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="Subject" class="form-control">
                        </div>
                        <div class="form-group">
                            <textarea name="" class="form-control" id="" cols="30" rows="10"></textarea>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary">Send</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
