<?php $url = Request::segment(1); ?>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <img src="{{ asset('/img/footer_logo.png') }}" width="150" alt="">
                <ul>
                </ul>
            </div>
            <div class="col-md-3">
                <ul>
                    <li>
                        <a class="no-underline hover:underline p-3 <?=  $url == 'home' || $url == '' ? 'active' : ''; ?>"
                           href="{{ route('page',['page' =>'home']) }}">HOME</a></li>
                    <li><a class="no-underline hover:underline p-3 <?=  $url == 'about' ? 'active' : ''; ?>"
                           href="{{ route('page',['page' =>'about']) }}">ABOUT</a></li>
                    <li><a class="no-underline hover:underline p-3" href="{{ route('page',['page' =>'product']) }}">PRODUCT</a>
                    </li>
{{--                    <li>+374 93 723093, +374 91 275751</li>--}}
                </ul>
            </div>
            <div class="col-md-3">
                <ul>
                    <li><a class="no-underline hover:underline p-3 <?=  $url == 'news' ? 'active' : ''; ?>"
                           href="{{ route('page',['page' =>'news']) }}">NEWS</a></li>
                    <li><a class="no-underline hover:underline p-3 <?=  $url == 'projects' ? 'active' : ''; ?>"
                           href="{{ route('page',['page' =>'projects']) }}">PROJECTS</a></li>
                    <li><a class="no-underline hover:underline p-3 <?=  $url == 'contact' ? 'active' : ''; ?>"
                           href="{{ route('page',['page' =>'contact']) }}">CONTACT</a></li>
{{--                    <li>simag.ainfo@gmail.com</li>--}}

                </ul>

            </div>
            <div class="col-md-3">
                <ul>
                    <li><a href=""><img src="{{ asset('/img/fb.png') }}" alt="">Facebook</a></li>
                    <li><a href=""><img src="{{ asset('/img/insta.png') }}" alt="">Instagram</a></li>

{{--                    <li> 7 Tadevosyan St, Yerevan, Armenia </li>--}}

                </ul>
            </div>
        </div>
    </div>
</footer>
