<?php $url = Request::segment(1); ?>
<header class="">
    <nav class="d-flex justify-content-between align-items-center">
        <div>
            <a href="{{ route('page', ['page' => 'home']) }}">
                <img src="{{ asset('/img/logo.png') }}" alt="" width="100">
            </a>
        </div>
        <div>
            <ul>
{{--                @foreach ($menu as $item)--}}
                    <li><a class="no-underline hover:underline p-3 <?=  $url == 'home' || $url == '' ? 'active' : ''; ?>" href="{{ route('page',['page' =>'home']) }}">HOME</a></li>
                    <li><a class="no-underline hover:underline p-3 <?=  $url == 'about' ? 'active' : ''; ?>" href="{{ route('page',['page' =>'about']) }}">ABOUT</a></li>
                    <li><a class="no-underline hover:underline p-3" href="{{ route('page',['page' =>'product']) }}">PRODUCT</a></li>
                    <li><a class="no-underline hover:underline p-3 <?=  $url == 'news' ? 'active' : ''; ?>" href="{{ route('page',['page' =>'news']) }}">NEWS</a></li>
                    <li><a class="no-underline hover:underline p-3 <?=  $url == 'projects' ? 'active' : ''; ?>" href="{{ route('page',['page' =>'projects']) }}">PROJECTS</a></li>
                    <li><a class="no-underline hover:underline p-3 <?=  $url == 'contact' ? 'active' : ''; ?>" href="{{ route('page',['page' =>'contact']) }}">CONTACT</a></li>

{{--                @endforeach--}}{{----}}
                {{--                <li><a href="{{route('page',['page' => 'home'])}}">HOME</a></li>--}}
                {{--                <li><a href="{{url('/about')}}">ABOUT</a></li>--}}
                {{--                <li><a href="">PRODUCT</a></li>--}}
                {{--                <li><a href="{{url('/news')}}">NEWS</a></li>--}}
                {{--                <li><a href="">SERVICES</a></li>--}}
                {{--                <li><a href="{{url('/contact')}}">CONTACT US</a></li>--}}
                <div class="lang_switcher">
                    @php
                        $uri = request()->path();
                        $exp = explode("/", $uri);
                        if(app()->getLocale() != config('localized-routes.omit_url_prefix_for_locale')){
                            array_shift($exp);
                        }
                    $path = implode("/", $exp);
                    $localizedPath = $path;
                    @endphp
                    <div onChange="window.location.href=this.value">

                        @foreach (config('localized-routes.supported-locales') as $lang => $language)
                            @php
                                if($language != config('localized-routes.omit_url_prefix_for_locale')){
                                    $localizedPath = $language . '/' . $path;
                                }
                            @endphp
                            <a
                            href="{{url($localizedPath)}}">
                                <img class="{{$lang == app()->getLocale() ? 'active' : ''}}" src="<?= asset('/img/'.$language.'.png') ?>" width="20" alt="">
                            </a>
                        @endforeach
                    </div>
                </div>
            </ul>
        </div>
    </nav>
</header>
