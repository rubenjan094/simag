<?php

namespace App;

trait PageTemplates
{
    /*
    |--------------------------------------------------------------------------
    | Page Templates for Backpack\PageManager
    |--------------------------------------------------------------------------
    |
    | Each page template has its own method, that define what fields should show up using the Backpack\CRUD API.
    | Use snake_case for naming and PageManager will make sure it looks pretty in the create/update form
    | template dropdown.
    |
    | Any fields defined here will show up after the standard page fields:
    | - select template
    | - page name (only seen by admins)
    | - page title
    | - page slug
    */

    private function home ()
    {


        $this->crud->addField([
            'name' => 'testiko',
            'label' => 'title',
            'fake' => true,
            'store_in' => 'content'
        ]);

        $this->crud->addField([
            'name' => 'aa',
            'label' => 'Aa',
            'fake' => true,
            'store_in' => 'content'
        ]);

    }

    private function about () {

    }

    private function news () {

    }

    private function contact () {

    }

    private function projects () {

    }
}
