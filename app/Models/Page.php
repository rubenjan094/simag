<?php
namespace App\Models;
use \Backpack\PageManager\app\Models\Page as BaseModel;
use App\Traits\CrudTrait;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;



class Page extends BaseModel
{
    use HasTranslations;

    protected $table = 'pages';

    protected $fakeColumns = ['content'];
    protected $fillable = ['template', 'name', 'title', 'slug', 'content', 'extras'];

    protected $casts = [
        'extras' => 'array',
    ];

    protected $translatable = ['extras','content'];


    public function getRouteParameters($locale = null)
    {
        return [
            $this->id,
            $this->getSlug($locale) // Add this method yourself of course :)
        ];
    }
//    public function setImageAttribute($value)
//    {
//        $attribute_name = "image";
//        $disk = config('backpack.base.root_disk_name');
//        $destination_path = "public/uploads/page/";
//        // if the image was erased
//        if ($value==null) {
//            // delete the image from disk
//            \Storage::disk($disk)->delete($this->{$attribute_name});
//            // set null in the database column
//            $this->attributes[$attribute_name] = null;
//        }
//
//        // if a base64 was sent, store it in the db
//        if (starts_with($value, 'data:image'))
//        {
//            // 0. Make the image
//            $image = \Image::make($value)->encode('jpg', 90);
//            // 1. Generate a filename.
//            $filename = md5($value.time()).'.jpg';
//            // 2. Store the image on disk.
//            $public_destination_path = \Str::replaceFirst('public/', '', $destination_path);
//            $this->attributes[$attribute_name] = $public_destination_path.'/'.$filename;
//            \Storage::disk($disk)->delete($this->{$attribute_name});
//            \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
//            // 3. Save the public path to the database
//            // but first, remove "public/" from the path, since we're pointing to it from the root folder
//            // that way, what gets saved in the database is the user-accesible URL
//
//        }
//    }
//
//    public function setAboutImageAttribute($value)
//    {
//        $attribute_name = "about_image";
//        $disk = config('backpack.base.root_disk_name');
//        $destination_path = "public/uploads/page/";
//        // if the image was erased
//        if ($value==null) {
//            // delete the image from disk
//            \Storage::disk($disk)->delete($this->{$attribute_name});
//
//            // set null in the database column
//            $this->attributes[$attribute_name] = null;
//        }
//
//        // if a base64 was sent, store it in the db
//        if (starts_with($value, 'data:image'))
//        {
//            // 0. Make the image
//            $image = \Image::make($value)->encode('jpg', 90);
//            // 1. Generate a filename.
//            $filename = md5($value.time()).'.jpg';
//            // 2. Store the image on disk.
//            \Storage::disk($disk)->delete($this->attributes[$attribute_name]);
//            \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
//            // 3. Save the public path to the database
//            // but first, remove "public/" from the path, since we're pointing to it from the root folder
//            // that way, what gets saved in the database is the user-accesible URL
//            $public_destination_path = \Str::replaceFirst('public/', '', $destination_path);
//            $this->attributes[$attribute_name] = $public_destination_path.'/'.$filename;
//        }
//    }
//
//    public static function boot()
//    {
//        parent::boot();
//        static::deleting(function($obj) {
//            if (count((array)$obj->images)) {
//                foreach ($obj->images as $file_path) {
//                    \Storage::disk(config('backpack.base.root_disk_name'))->delete($file_path);
//                }
//            }
//        });
//    }
}
