<?php

namespace App\Http\Controllers;

use App\Models\MenuItem;
use Backpack\PageManager\app\Models\Page;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public function index($slug = 'home', $subs = null)
    {

        if(app()->getLocale() != config('localized-routes.omit_url_prefix_for_locale')){
            $slug = request()->segment(2);
        } else if(!empty(request()->segment(1))){
            $slug = request()->segment(1);
        }


        $slug = !empty($slug) ? $slug : 'home';

        $page = Page::findBySlug($slug);

        if (!$page)
        {
            abort(404, 'Please go back to our <a href="'.url('').'">homepage</a>.');
        }

        $this->data['title'] = $page->title;
        $this->data['page'] = $page->withFakes();


        return view('pages.'.$page->template, $this->data);
    }
}
