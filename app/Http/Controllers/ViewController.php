<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class ViewController extends Controller
{
    public function index () {
        return view('pages.home');
    }

    public function about () {
        return view('pages.about');
    }

    public function users () {
        return view('users');
    }

    public function contact () {
        return view('pages.contact');
    }

    public function news () {
        return view('pages.news');
    }
}
