<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;

class Users extends Component
{
    public $amount = 10;

    public function load () {
        $this->amount += 10;
    }

    public function render()
    {
        $users = User::take($this->amount)->get();
        return view('livewire.users', compact('users'));
    }
}
