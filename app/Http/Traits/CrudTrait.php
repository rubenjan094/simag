<?php

namespace App\Traits;

use Backpack\CRUD\app\Models\Traits\CrudTrait as OriginalCrudTrait;

trait CrudTrait
{
    use OriginalCrudTrait;

    public function addFakes($columns = ['extras'])
    {
        foreach ($columns as $key => $column) {
            if (! isset($this->attributes[$column])) {
                continue;
            }

            if ($this->translationEnabled() && $this->isTranslatableAttribute($column)) {
                $column_contents = $this->{$column};
            } else {
                $column_contents = $this->attributes[$column];
            }

            if (! is_object($column_contents)) {
                $column_contents = json_decode($column_contents);
            }

            if ((is_array($column_contents) || is_object($column_contents) || $column_contents instanceof Traversable)) {
                foreach ($column_contents as $fake_field_name => $fake_field_value) {
                    $this->setAttribute($fake_field_name, $fake_field_value);
                }
            }
        }
    }
}
