<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::localized(function () {

    Route::get('{page}/{subs?}', ['uses' => '\App\Http\Controllers\PageController@index'])
        ->where(['page' => '^(((?=(?!admin))(?=(?!\/)).))*$', 'subs' => '.*'])->name('page');

});


//Route::get('/', [\App\Http\Controllers\ViewController::class,'index']);
//Route::get('/about',[\App\Http\Controllers\ViewController::class,'about']);
//Route::get('/users',[\App\Http\Controllers\ViewController::class,'users']);
//Route::get('/contact', [\App\Http\Controllers\ViewController::class,'contact']);
//Route::get('/news',[\App\Http\Controllers\ViewController::class,'news']);
